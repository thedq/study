以下是一些截图：

==用户可视界面==

- 登录页面

![登录](https://gitee.com/liu-kunyu/study/raw/master/README.assets/登录-1632128922101.png)

- 首页：

![用户端首页](https://gitee.com/liu-kunyu/study/raw/master/README.assets/用户端首页-1632128929981.png)

- 店铺查找页面：

![店铺查找](https://gitee.com/liu-kunyu/study/raw/master/README.assets/店铺查找-1632128942718.png)



- 店铺页面

![店铺首页](https://gitee.com/liu-kunyu/study/raw/master/README.assets/店铺首页-1632128950196.png)

==店铺管理者可视页面==

- 店铺页面

![店铺管理员店铺页面](https://gitee.com/liu-kunyu/study/raw/master/README.assets/店铺管理员店铺页面-1632128964261.png)

- 店铺管理员商品管理

![店铺管理员商品管理](https://gitee.com/liu-kunyu/study/raw/master/README.assets/店铺管理员商品管理-1632128971078.png)

==系统管理员可视页面==

- 管理员店铺管理

![管理员店铺管理](https://gitee.com/liu-kunyu/study/raw/master/README.assets/管理员店铺管理-1632128981558.png)

- 管理员用户管理

![管理员用户管理](https://gitee.com/liu-kunyu/study/raw/master/README.assets/管理员用户管理-1632128992958.png)



==还未完善的点==

- 一些重复操作的增删查改
- 店铺：
  - 商品查询
  - 评价

- echarts可视化展示，目前只做了店铺里面的商品销量demo
- 安全性校验和拦截都还没做，比如登录拦截、权限验证等
- 。。。

